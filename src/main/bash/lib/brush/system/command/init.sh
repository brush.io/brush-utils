#!/usr/bin/bash

################################################################################
[[ -n "${__LIB_BRUSH_SYSTEM_INIT_SH__}" ]] && return
readonly __LIB_BRUSH_SYSTEM_INIT_SH__=1
################################################################################

function system::init() {
	system::init._environment
	system::init._structure

	pkg import
	cron daily cache clean
	cron daily pkg update
}

function system::init._environment() {
	local must_restart=false

	#-- MSYS2
	if [[ ! "${CHERE_INVOKING}" ]]; then
		environment set CHERE_INVOKING 1
		must_restart=true
	fi

	if [[ ! "${MSYSTEM}" ]]; then
		environment set MSYSTEM "MINGW64"
		must_restart=true
	fi

	if [[ ! "${MSYS}" ]]; then
		environment set MSYS "winsymlinks:nativestrict"
		must_restart=true
	fi

	if [[ ! "${MSYS2_PATH_TYPE}" ]]; then
		environment set MSYS2_PATH_TYPE "inherit"
		must_restart=true
	fi

	#-- Windows
	# FIXME: Must check with flattened value ['%LocalAppData%\Programs' = 'C:\users...\Programs'].
	#environment set ProgramLocalFiles '%LocalAppData%\Programs' || must_restart=true

	#-- Restart Terminal
	if [[ "${must_restart}" == true ]]; then
		msg.warn "❗ Environment variables have been changed, must restart the terminal"
		read -r
		kill -9 "${PPID}"
	fi
}

function system::init._structure() {
	local -r directories=(
		"${XDG_CONFIG_HOME}"
		"${XDG_CACHE_HOME}"
		"${XDG_LOCAL_HOME}"
		"${XDG_BIN_HOME}"
		"${XDG_DATA_HOME}"
		"${XDG_STATE_HOME}"
		"/win32/bin"
		"/win32/etc"
		"/win32/include"
		"/win32/lib"
		"/win32/opt"
		"/win32/share"
	)

	for directrory in "${directories[@]}"; do
		mkdir --parents "${directrory}"
	done
}
